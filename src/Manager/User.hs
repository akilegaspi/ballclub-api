{-# LANGUAGE NoImplicitPrelude #-}

module Manager.User where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.User as DB
import Model
import Manager

applyTime :: MonadIO m => (UTCTime -> User) -> ManagerT m User
applyTime userF = userF <$> (liftIO getCurrentTime)

getUsers :: MonadIO m => ManagerT m [Entity User]
getUsers = runDb DB.getAllUsers

newUser :: MonadIO m => (UTCTime -> User) -> ManagerT m UserId
newUser userF = do
  user <- applyTime userF
  userId <- runDb $ DB.newUserEntity user
  return userId

getUser :: MonadIO m => UserId -> ManagerT m (Maybe (Entity User))
getUser userId = runDb $ DB.getUserEntity userId

updateUser :: MonadIO m => UserId -> (UTCTime -> User) -> ManagerT m ()
updateUser userId userF = do
  user <- applyTime userF
  runDb $ DB.updateUserEntity userId user

deleteUser :: MonadIO m => UserId -> ManagerT m ()
deleteUser userId = runDb $ DB.deleteUserEntity userId
