{-# LANGUAGE NoImplicitPrelude #-}

module Manager.Game where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.Game as DB
import Model
import Manager

getGames :: MonadIO m => ManagerT m [Entity Game]
getGames = runDb DB.getAllGames

newGame :: MonadIO m => Game -> ManagerT m GameId
newGame game = do
  gameId <- runDb $ DB.newGameEntity game
  return gameId

getGame :: MonadIO m => GameId -> ManagerT m (Maybe (Entity Game))
getGame gameId = runDb $ DB.getGameEntity gameId

updateGame :: MonadIO m => GameId -> Game -> ManagerT m ()
updateGame gameId game =
  runDb $ DB.updateGameEntity gameId game

deleteGame :: MonadIO m => GameId -> ManagerT m ()
deleteGame gameId = runDb $ DB.deleteGameEntity gameId
