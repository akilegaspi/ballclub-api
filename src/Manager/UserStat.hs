{-# LANGUAGE NoImplicitPrelude #-}

module Manager.UserStat where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.UserStat as DB
import Model
import Manager

getUserstats :: MonadIO m => ManagerT m [Entity UserStat]
getUserstats = runDb DB.getAllUserStats

newUserstat :: MonadIO m => UserStat -> ManagerT m UserStatId
newUserstat userstat = do
  userstatId <- runDb $ DB.newUserStatEntity userstat
  return userstatId

getUserstat :: MonadIO m => UserStatId -> ManagerT m (Maybe (Entity UserStat))
getUserstat userstatId = runDb $ DB.getUserStatEntity userstatId

updateUserstat :: MonadIO m => UserStatId -> UserStat -> ManagerT m ()
updateUserstat userstatId userstat =
  runDb $ DB.updateUserStatEntity userstatId userstat

deleteUserstat :: MonadIO m => UserStatId -> ManagerT m ()
deleteUserstat userstatId = runDb $ DB.deleteUserStatEntity userstatId

