{-# LANGUAGE NoImplicitPrelude #-}

module Manager.Location where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.Location as DB
import Model
import Manager

getLocations :: MonadIO m => ManagerT m [Entity Location]
getLocations = runDb DB.getAllLocations

newLocation :: MonadIO m => Location -> ManagerT m LocationId
newLocation location = do
  locationId <- runDb $ DB.newLocationEntity location
  return locationId

getLocation :: MonadIO m => LocationId -> ManagerT m (Maybe (Entity Location))
getLocation locationId = runDb $ DB.getLocationEntity locationId

updateLocation :: MonadIO m => LocationId -> Location -> ManagerT m ()
updateLocation locationId location =
  runDb $ DB.updateLocationEntity locationId location

deleteLocation :: MonadIO m => LocationId -> ManagerT m ()
deleteLocation locationId = runDb $ DB.deleteLocationEntity locationId
