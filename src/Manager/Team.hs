{-# LANGUAGE NoImplicitPrelude #-}

module Manager.Team where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.Team as DB
import Model
import Manager

getTeams :: MonadIO m => ManagerT m [Entity Team]
getTeams = runDb DB.getAllTeams

newTeam :: MonadIO m => Team -> ManagerT m TeamId
newTeam team = do
  teamId <- runDb $ DB.newTeamEntity team
  return teamId

getTeam :: MonadIO m => TeamId -> ManagerT m (Maybe (Entity Team))
getTeam teamId = runDb $ DB.getTeamEntity teamId

updateTeam :: MonadIO m => TeamId -> Team -> ManagerT m ()
updateTeam teamId team =
  runDb $ DB.updateTeamEntity teamId team

deleteTeam :: MonadIO m => TeamId -> ManagerT m ()
deleteTeam teamId = runDb $ DB.deleteTeamEntity teamId
