{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}

module Manager.Auth where

import ClassyPrelude hiding (pack)
import Control.Monad.Except (MonadError)
import Data.ByteString.Char8 (pack)
import qualified Data.ByteString.Lazy.Char8 as L8
import qualified Data.ByteString.Lazy as BL
import Crypto.KDF.BCrypt

import Config
import Manager
import qualified Model as M
import qualified Types as T
import Util

import Database.Persist
import Servant
import Servant.Auth.Server

eitherToManager :: (Show a1, MonadError ServantErr m) =>
                   Either a1 t ->
                   (t -> a2) ->
                   ServantErr ->
                   m a2
eitherToManager (Left v) _ onFail = throwError $ onFail { errBody = (L8.pack.show) v }
eitherToManager (Right v) onSuccess _ = return $ onSuccess v

generateToken :: MonadIO m => M.User -> ManagerT m Text
generateToken user = do
  jws <- asks jwtSettings
  etoken <- liftIO $ makeJWT user jws Nothing
  token <- eitherToManager etoken (decodeUtf8 . BL.toStrict) err500
  return token

login :: MonadIO m => T.RequestLogin -> ManagerT m T.ResponseLogin
login (T.RequestLogin (T.LoginBody username password)) = do
  (Entity _ user) <- notFoundIfNothing =<< M.runDb (M.QueryT $ getBy (M.Username username))
  unless
    (validatePassword
      ((pack . unpack) (M.userPassword user))
      ((pack . unpack) (password))) $
    throwError err401 { errBody = encodeRespError "Invalid Username\\Password" }
  token <- generateToken user
  return $
    T.ResponseLogin $
    T.UserBody
    (M.userUsername user)
    (M.userEmail user)
    token
    (M.userProfileImage user)
  where
    notFoundIfNothing Nothing =
      throwError err401 { errBody = encodeRespError "You're not allowed to access this" }
    notFoundIfNothing (Just v) = return v
