{-# LANGUAGE NoImplicitPrelude #-}

module Manager.League where

import Control.Applicative ((<$>), pure)
import Control.Monad.IO.Class (MonadIO)

import ClassyPrelude
import Database.Persist.Types (Entity(..))

import qualified DB.League as DB
import Model
import Manager

getLeagues :: MonadIO m => ManagerT m [Entity League]
getLeagues = runDb DB.getAllLeagues

newLeague :: MonadIO m => League -> ManagerT m LeagueId
newLeague league = do
  leagueId <- runDb $ DB.newLeagueEntity league
  return leagueId

getLeague :: MonadIO m => LeagueId -> ManagerT m (Maybe (Entity League))
getLeague leagueId = runDb $ DB.getLeagueEntity leagueId

updateLeague :: MonadIO m => LeagueId -> League -> ManagerT m ()
updateLeague leagueId league =
  runDb $ DB.updateLeagueEntity leagueId league

deleteLeague :: MonadIO m => LeagueId -> ManagerT m ()
deleteLeague leagueId = runDb $ DB.deleteLeagueEntity leagueId
