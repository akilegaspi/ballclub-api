{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE KindSignatures    #-}
{-# LANGUAGE RankNTypes        #-}
{-# LANGUAGE TemplateHaskell   #-}

module Types where

import ClassyPrelude

import Data.Aeson
import Data.Aeson.Casing

type Username = Text
type Password = Text

aesonFormat :: Options
aesonFormat = aesonPrefix camelCase

data RegistrationBody = RegistrationBody
  { regUsername  :: Username
  , regPassword  :: Password
  , regFirstName :: Text
  , regLastName  :: Text
  , regEmail     :: Text
  , regBirthday  :: UTCTime }
  deriving (Generic)

instance FromJSON RegistrationBody where
  parseJSON = genericParseJSON aesonFormat

newtype RequestRegistration = RequestRegistration
  { reqRegBody :: RegistrationBody }
  deriving (Generic)

instance FromJSON RequestRegistration where
  parseJSON = genericParseJSON aesonFormat

data LoginBody = LoginBody
    { loginUsername :: Username
    , loginPassword :: Password
    } deriving (Generic)

instance FromJSON LoginBody where
  parseJSON = genericParseJSON aesonFormat

newtype RequestLogin = RequestLogin
  { reqLoginBody :: LoginBody }
  deriving (Generic)

instance FromJSON RequestLogin where
  parseJSON = genericParseJSON aesonFormat

data UserBody = UserBody
  { userUsername :: Text
  , userEmail    :: Text
  , userToken    :: Text
  , userImage    :: Text
  } deriving (Generic)

instance ToJSON UserBody where
  toJSON = genericToJSON aesonFormat

newtype ResponseRegistration = ResponseRegistration
  { resRegBody ::UserBody }
  deriving (Generic)

newtype ResponseLogin = ResponseLogin
  { resLoginBody :: UserBody }
  deriving (Generic)

newtype ErrorBody = ErrorBody
  { errErrorBody :: Text }
  deriving (Generic)

instance ToJSON ErrorBody where
  toJSON = genericToJSON aesonFormat

newtype ResponseError = ResponseError
  { resErrBody :: ErrorBody }
  deriving (Generic)

instance ToJSON ResponseError where
  toJSON = genericToJSON aesonFormat
