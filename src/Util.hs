{-# LANGUAGE NoImplicitPrelude #-}

module Util where

import ClassyPrelude
import qualified Data.ByteString.Lazy as BL

import Types

import Data.Aeson (encode)

encodeRespError :: Text -> BL.ByteString
encodeRespError = encode . ResponseError . ErrorBody
