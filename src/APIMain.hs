{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE DataKinds         #-}

module APIMain
    ( runServer,
      develMain
    ) where

import Import

import Database.Persist.Sql (runMigration, runSqlPool)
import Database.Persist.Postgresql (createPostgresqlPool, pgConnStr, pgPoolSize)

import Control.Monad.Logger (runStdoutLoggingT)
import Data.Yaml.Config (loadYamlSettingsArgs, useEnv)
import Servant.Server
import Servant.Auth.Server (generateKey, defaultJWTSettings, defaultCookieSettings, CookieSettings, JWTSettings)
import Network.Wai.Handler.Warp (run)

makeFoundation :: AppConfig -> IO (App, Context [CookieSettings, JWTSettings])
makeFoundation appConfig = do
  let mkFoundation appConnPool jwtSettings = App {..}
  jwk <- generateKey
  pool <- runStdoutLoggingT (createPostgresqlPool
          (pgConnStr $ appDatabaseConf appConfig)
          (pgPoolSize $ appDatabaseConf appConfig))
  let jws = defaultJWTSettings jwk
      cfg = defaultCookieSettings :. jws :. EmptyContext
  runSqlPool (runMigration migrateAll) pool
  return $ (mkFoundation pool jws, cfg)

getAppConfig :: IO AppConfig
getAppConfig = loadYamlSettingsArgs [configYmlValue] useEnv

appMain :: IO ()
appMain = do
  config <- getAppConfig
  (foundation, context) <- makeFoundation config
  let apiServer = serveWithContext allApi context $ server foundation
  run (appPort config) apiServer

develMain :: IO (Int, Application)
develMain = do
  config <- getAppConfig
  (foundation, context) <- makeFoundation config
  let apiServer = serveWithContext allApi context $ server foundation
  return ((appPort config), apiServer)

runServer :: IO ()
runServer = appMain
  
