{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Model where

import           Control.Monad.Logger                  (NoLoggingT)
import           Control.Monad.Trans.Reader            (ReaderT)
import           Control.Monad.Trans.Resource.Internal (ResourceT)

-- Persistent Modules
import Database.Persist.TH
import Database.Persist.Quasi
import Database.Persist.Sql

-- Data Types
import ClassyPrelude
import Servant.Auth.Server

import Config

-- | We generate a bunch of Models from @config/models@
-- | via Template Haskell with splicing since
-- | Persistence Package provides Meta/Macro Programming
-- | Helpers
-- | The result is then embedded to the compiled code hence
-- | No IO action is needed to perform such extraction and
-- | splicing of data
share [mkPersist sqlSettings, mkMigrate "migrateAll"]
    $(persistFileWith lowerCaseSettings "config/models")

data Auths (auths :: [*]) val

instance ToJWT User
instance FromJWT User

newtype QueryT m a = QueryT { runQueryT :: ReaderT SqlBackend (NoLoggingT (ResourceT m)) a }
                   deriving (Functor, Applicative, Monad)
type Query a = QueryT IO a

runDb :: (MonadIO m, MonadReader App m) => Query a -> m a
runDb query = do
  pool <- asks appConnPool
  liftSqlPersistMPool (runQueryT query) pool
