{-# LANGUAGE NoImplicitPrelude #-}

module DB.Team where

import           ClassyPrelude                  hiding (delete, id)

import Manager

import Database.Persist.Sql
import Model

getAllTeams :: Query [Entity Team]
getAllTeams = QueryT $ selectList [] []

newTeamEntity :: Team -> Query TeamId
newTeamEntity team = QueryT $ insert team

getTeamEntity :: TeamId -> Query (Maybe (Entity Team))
getTeamEntity id = QueryT $ selectFirst [TeamId ==. id] []

updateTeamEntity :: TeamId -> Team -> Query ()
updateTeamEntity teamId team = QueryT $ replace teamId team

deleteTeamEntity :: TeamId -> Query ()
deleteTeamEntity teamId = QueryT $ delete teamId
