{-# LANGUAGE NoImplicitPrelude #-}

module DB.Location where

import           ClassyPrelude                  hiding (delete, id)

import Manager

import Database.Persist.Sql
import Model

getAllLocations :: Query [Entity Location]
getAllLocations = QueryT $ selectList [] []

newLocationEntity :: Location -> Query LocationId
newLocationEntity location = QueryT $ insert location

getLocationEntity :: LocationId -> Query (Maybe (Entity Location))
getLocationEntity id = QueryT $ selectFirst [LocationId ==. id] []

updateLocationEntity :: LocationId -> Location -> Query ()
updateLocationEntity locationId location = QueryT $ replace locationId location

deleteLocationEntity :: LocationId -> Query ()
deleteLocationEntity locationId = QueryT $ delete locationId

