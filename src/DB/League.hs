{-# LANGUAGE NoImplicitPrelude #-}

module DB.League where

import           ClassyPrelude                  hiding (delete, id)

import           Control.Monad.Logger                  (NoLoggingT)
import           Control.Monad.Trans.Reader            (ReaderT)
import           Control.Monad.Trans.Resource.Internal (ResourceT)

import Database.Persist.Sql
import Model

getAllLeagues :: Query [Entity League]
getAllLeagues = QueryT $ selectList [] []

newLeagueEntity :: League -> Query LeagueId
newLeagueEntity league = QueryT $ insert league

getLeagueEntity :: LeagueId -> Query (Maybe (Entity League))
getLeagueEntity id = QueryT $ selectFirst [LeagueId ==. id] []

updateLeagueEntity :: LeagueId -> League -> Query ()
updateLeagueEntity leagueId league = QueryT $ replace leagueId league

deleteLeagueEntity :: LeagueId -> Query ()
deleteLeagueEntity leagueId = QueryT $ delete leagueId
