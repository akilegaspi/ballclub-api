{-# LANGUAGE NoImplicitPrelude #-}

module DB.Game where

import           ClassyPrelude                  hiding (delete, id)

import           Control.Monad.Logger                  (NoLoggingT)
import           Control.Monad.Trans.Reader            (ReaderT)
import           Control.Monad.Trans.Resource.Internal (ResourceT)

import Database.Persist.Sql
import Model

getAllGames :: Query [Entity Game]
getAllGames = QueryT $ selectList [] []

newGameEntity :: Game -> Query GameId
newGameEntity game = QueryT $ insert game

getGameEntity :: GameId -> Query (Maybe (Entity Game))
getGameEntity id = QueryT $ selectFirst [GameId ==. id] []

updateGameEntity :: GameId -> Game -> Query ()
updateGameEntity gameId game = QueryT $ replace gameId game

deleteGameEntity :: GameId -> Query ()
deleteGameEntity gameId = QueryT $ delete gameId
