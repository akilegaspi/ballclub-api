{-# LANGUAGE NoImplicitPrelude #-}

module DB.UserStat where

import           ClassyPrelude                  hiding (delete, id)

import Manager

import Database.Persist.Sql
import Model

getAllUserStats :: Query [Entity UserStat]
getAllUserStats = QueryT $ selectList [] []

newUserStatEntity :: UserStat -> Query UserStatId
newUserStatEntity userstat = QueryT $ insert userstat

getUserStatEntity :: UserStatId -> Query (Maybe (Entity UserStat))
getUserStatEntity id = QueryT $ selectFirst [UserStatId ==. id] []

updateUserStatEntity :: UserStatId -> UserStat -> Query ()
updateUserStatEntity userstatId userstat = QueryT $ replace userstatId userstat

deleteUserStatEntity :: UserStatId -> Query ()
deleteUserStatEntity userstatId = QueryT $ delete userstatId
