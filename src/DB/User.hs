{-# LANGUAGE NoImplicitPrelude #-}

module DB.User where

import           ClassyPrelude                  hiding (delete, id)

import           Control.Monad.Logger                  (NoLoggingT)
import           Control.Monad.Trans.Reader            (ReaderT)
import           Control.Monad.Trans.Resource.Internal (ResourceT)

import Database.Persist.Sql
import Model

getAllUsers :: Query [Entity User]
getAllUsers = QueryT $ selectList [] []

newUserEntity :: User -> Query UserId
newUserEntity user = QueryT $ insert user

getUserEntity :: UserId -> Query (Maybe (Entity User))
getUserEntity id = QueryT $ selectFirst [UserId ==. id] []

updateUserEntity :: UserId -> User -> Query ()
updateUserEntity userId user = QueryT $ replace userId user

deleteUserEntity :: UserId -> Query ()
deleteUserEntity userId = QueryT $ delete userId

getUserByLogin :: Text -> Text -> Query (Maybe (Entity User))
getUserByLogin username _ = QueryT $ selectFirst [UserUsername ==. username] []
