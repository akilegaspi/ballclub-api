{-# LANGUAGE CPP               #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
-- Central Settings aggregator for all application config

module Settings where

import ClassyPrelude

-- Control structures
import qualified Control.Exception as Exception

-- Settings getter and parser
import Data.FileEmbed                         (embedFile)
import Data.Yaml                              (decodeEither', Value, FromJSON(..), (.:))
import Data.Yaml.Config                       (applyEnvValue)
import Data.Aeson                             (Result (..), fromJSON, withObject,
                                               (.!=), (.:?))
import Database.Persist.Postgresql            (PostgresConf)
import Network.Wai.Handler.Warp               (HostPreference)

data AppConfig = AppConfig
    {
      -- Database Connectivity Config
      appDatabaseConf    :: PostgresConf
      -- Interface where we should bind our application
    , appHost            :: HostPreference
      -- URI where the API should be served
    , appRoot            :: Text
      -- Port where the API will listen to
    , appPort            :: Int
      -- If we get the IP address from the header for logging since
      -- we're going to be sitting behind a reverse-proxy to serve
      -- the requests [Conditional]
    , appIpFromHeader    :: Bool
      -- Specifies if we log everything or just info logs
    , appLogAll          :: Bool
      -- Would specify better details for our logs
    , appDetailedLogging :: Bool
      -- The secret key we would consume for securing requests/responses
    , appSecretKey       :: Maybe Text

    , appLogFile         :: Maybe FilePath
    }

-- | We create an instance to parse our Application Config
-- | from YAML via Data.Yaml and Data.Aeson utilities
-- | since they are both using the same typeclasses to do so.
instance FromJSON AppConfig where
    parseJSON = withObject "AppConfig" $ \o -> do
        let defaultDev =
#ifdef DEVELOPMENT
                True
#else
                False
#endif
        appDatabaseConf <- o .: "database"
        -- We parse the IPv4 Interface that we're going to use
        appHost                 <- fromString <$> o .: "host"
        appRoot                 <- o .: "approot"
        appPort                 <- o .: "port"
        appIpFromHeader         <- o .: "ip-from-header"
        dev                     <- o .:? "development" .!= defaultDev
        appLogAll               <- o .: "log-all"      .!= dev
        appSecretKey            <- o .:? "app-secret"
        appLogFile              <- o .:? "app-log-file"
        appDetailedLogging      <- o .: "app-detailed-logging"
              
        return AppConfig {..}


-- | Getting the bytes of the @config/settings.yml@.
-- | This is a pure function that reads the @config/settings.yml@ file
-- | on compile so that no IO action is done when pulling the config
configYmlBS :: ByteString
configYmlBS = $(embedFile "config/settings.yml")

-- | Converting @config/settings.yml@'s bytes into parsable value
configYmlValue :: Value
configYmlValue = either Exception.throw id
                 $ decodeEither' configYmlBS

-- | @AppConfig@ parsed during compile time from @config/settings.yml@.
compileTimeAppConfig :: AppConfig
compileTimeAppConfig =
  case fromJSON $ applyEnvValue False mempty configYmlValue of
    Error e -> error e
    Success settings -> settings
