{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE RecordWildCards            #-}

module Foundation (allApi, App(..), server)
where

import ClassyPrelude hiding (Handler)
import Control.Monad.Except (ExceptT, MonadError)

import Import.Block
import Database.Persist.Sql (ConnectionPool)
import Config
import Manager

import Servant
import Servant.Auth
import Servant.Auth.Server

import API.AuthAPI
import API.UserAPI
import API.GameAPI
import API.TeamAPI
import API.LeagueAPI
import API.LocationAPI
import API.UserStatAPI


type AllAPI auth = "users" :> (Auth auth User :> UserAPI)
         :<|> "games" :> (Auth auth User :> GameAPI)
         :<|> "teams" :> TeamAPI
         :<|> "userstats" :> (Auth auth User :> UserStatAPI)
         :<|> "league" :> LeagueAPI
         :<|> "location" :> LocationAPI

serverT :: MonadIO m => ServerT (AllAPI auth) (ManagerT m)
serverT = userApi :<|> gameApi :<|> teamApi :<|> userStatApi :<|> leagueApi :<|> locationApi

server :: App -> Server (AllAPI '[JWT])
server appConfig = hoistServerWithContext
                   allApi
                   ctxProxy
                   (managerToHandler appConfig) serverT
  where
    ctxProxy = Proxy :: Proxy '[CookieSettings, JWTSettings]
allApi :: Proxy (AllAPI '[JWT])
allApi = Proxy
