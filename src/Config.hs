{-# LANGUAGE NoImplicitPrelude #-}

module Config where

import Database.Persist.Sql
import Servant.Auth.Server

import Settings

data App = App
  { appConfig :: AppConfig
  , appConnPool :: ConnectionPool
  , jwtSettings :: JWTSettings
  }
