{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Manager where

import ClassyPrelude hiding (Handler)
import Control.Monad.Except (ExceptT, MonadError)

import Servant

import Config

newtype ManagerT m a = ManagerT
  { runManager :: ReaderT App (ExceptT ServantErr m) a }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadIO
           , MonadReader App
           , MonadError ServantErr )

type Manager a = ManagerT IO a

managerToHandler :: App -> Manager a -> Handler a
managerToHandler app man = Handler $ flip runReaderT app $ runManager man 
