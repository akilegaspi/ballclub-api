{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module API.LeagueAPI where

import ClassyPrelude hiding (delete, id)
import Control.Monad.Logger (NoLoggingT)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.Resource.Internal (ResourceT)
import Data.Aeson (FromJSON)

-- API Creation Modules
import Backbone (CrudAPI, mkCrud)

-- Database Modules
import Database.Persist.Sql
import Manager
import qualified Manager.League as M
import Model

-- Server Type
import Servant.Server (ServerT, errBody)
import Servant.API

data NewLeague = NewLeague { timeScheduled :: UTCTime
                              , userId :: UserId
                              , locationId :: LocationId
                              } deriving (Eq, Show, Generic)
instance FromJSON NewLeague

toLeague :: UTCTime -> NewLeague -> League
toLeague time NewLeague{..} = League userId timeScheduled locationId time

type LeagueAPI = LeagueCrudAPI
type LeagueCrudAPI = CrudAPI NewLeague League LeagueId

leagueApi :: MonadIO m => ServerT LeagueCrudAPI (ManagerT m)
leagueApi = mkCrud getLeagues newLeague getLeague updateLeague deleteLeague
    where
        getLeagues :: MonadIO m => ManagerT m [Entity League]
        getLeagues = M.getLeagues

        newLeague :: MonadIO m => NewLeague -> ManagerT m NoContent
        newLeague league = do
          time <- liftIO getCurrentTime
          let nLeague = toLeague time league
          _ <- M.newLeague nLeague
          return NoContent
                        
        getLeague :: MonadIO m => LeagueId -> ManagerT m (Maybe (Entity League))
        getLeague id = M.getLeague id

        updateLeague :: MonadIO m => LeagueId -> NewLeague -> ManagerT m NoContent
        updateLeague id updatedLeague = do
          time <- liftIO getCurrentTime
          let uLeague = toLeague time updatedLeague
          _ <- M.newLeague uLeague
          return NoContent

        deleteLeague :: MonadIO m => LeagueId -> ManagerT m NoContent
        deleteLeague id = do
          _ <- M.deleteLeague id
          return NoContent
