{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module API.TeamAPI where

import ClassyPrelude hiding (delete, id)
import Data.Aeson (FromJSON)
-- API Creation Modules
import Backbone (CrudAPI, mkCrud)

-- Database Modules
import Manager
import qualified Manager.Team as M
import Database.Persist.Sql
import Model

-- Server Type
import Servant.Server (ServerT)
import Servant.API

newtype NewTeam = NewTeam {name :: Text}
                  deriving (Eq, Show, Generic)

instance FromJSON NewTeam

type TeamAPI = TeamCrudAPI
type TeamCrudAPI = CrudAPI NewTeam Team TeamId

toTeam :: NewTeam -> Team
toTeam NewTeam{..} = Team name 0 0

teamApi :: MonadIO m => ServerT TeamCrudAPI (ManagerT m)
teamApi = mkCrud getTeams newTeam getTeam updateTeam deleteTeam
    where
        getTeams :: MonadIO m => ManagerT m [Entity Team]
        getTeams = M.getTeams

        newTeam :: MonadIO m => NewTeam -> ManagerT m NoContent
        newTeam team = do
          let nTeam = toTeam team
          _ <- M.newTeam nTeam
          return NoContent
                        
        getTeam :: MonadIO m => TeamId -> ManagerT m (Maybe (Entity Team))
        getTeam id = M.getTeam id
          
        updateTeam :: MonadIO m => TeamId -> NewTeam -> ManagerT m NoContent
        updateTeam id updatedTeam = do
          let uTeam = toTeam updatedTeam
          M.updateTeam id uTeam
          return NoContent

        deleteTeam :: MonadIO m => TeamId -> ManagerT m NoContent
        deleteTeam id = do
          M.deleteTeam id
          return NoContent
