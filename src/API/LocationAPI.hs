{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module API.LocationAPI where

import ClassyPrelude hiding (delete, id)
import Control.Monad.Logger (NoLoggingT)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.Resource.Internal (ResourceT)
import Data.Aeson (FromJSON)

-- API Creation Modules
import Backbone (CrudAPI, mkCrud)

-- Database Modules
import Database.Persist.Sql
import Manager
import qualified Manager.Location as M
import Model

-- Server Type
import Servant.Server (ServerT)
import Servant.API

data NewLocation = NewLocation { name :: Text
                               , posX :: Double
                               , posY :: Double
                               } deriving (Eq, Show, Generic)
instance FromJSON NewLocation

toLocation :: NewLocation -> Location
toLocation NewLocation{..} = Location name posX posY False

type LocationAPI = LocationCrudAPI
type LocationCrudAPI = CrudAPI NewLocation Location LocationId

locationApi :: MonadIO m => ServerT LocationCrudAPI (ManagerT m)
locationApi = mkCrud getLocations newLocation getLocation updateLocation deleteLocation
    where
        getLocations :: MonadIO m => ManagerT m [Entity Location]
        getLocations = M.getLocations

        newLocation :: MonadIO m => NewLocation -> ManagerT m NoContent
        newLocation location = do
          let nLocation = toLocation location
          _ <- M.newLocation nLocation
          return NoContent
                        
        getLocation :: MonadIO m => LocationId ->  ManagerT m (Maybe (Entity Location))
        getLocation id = M.getLocation id

        updateLocation :: MonadIO m => LocationId -> NewLocation -> ManagerT m NoContent
        updateLocation id updatedLocation = do
          let uLocation = toLocation updatedLocation
          M.updateLocation id uLocation
          return NoContent

        deleteLocation :: MonadIO m => LocationId -> ManagerT m NoContent
        deleteLocation id = do
          M.deleteLocation id
          return NoContent
          
