{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}

module API.UserAPI where

import           ClassyPrelude                  hiding (delete, id)
import           Data.Aeson (FromJSON)

-- API Creation Modules
import Backbone (CrudAPI, mkCrud)
import Manager
import qualified Manager.User as M

-- Database Modules
import Database.Persist.Sql
import Model (User(..), UserId)

-- Server Type
import Servant.Server (ServerT)
import Servant.API
import Servant.Auth.Server

data NewUser = NewUser { newUsername     :: Text
                       , newPassword     :: Text
                       , newFirstName    :: Text
                       , newLastName     :: Text
                       , newEmail        :: Text
                       , newBirthday     :: Maybe UTCTime
                       , newPosition     :: Maybe String
                       , newProfileImage :: Maybe Text
                       , newAddress      :: Maybe Text
                       } deriving (Eq, Show, Generic)

instance FromJSON NewUser

toUser :: NewUser -> UTCTime -> User
toUser NewUser{..} time = let
  profImage = case newProfileImage of
                Just pImage -> pImage
                Nothing -> "Placeholder"
  in User newUsername
          newPassword
          newFirstName
          newLastName
          newEmail
          newBirthday
          newPosition
          profImage
          newAddress
          Nothing
          False
          time
          Nothing

type UserAPI = UserCrudAPI
type UserCrudAPI = CrudAPI NewUser User UserId

userApi :: MonadIO m => AuthResult User -> ServerT UserCrudAPI (ManagerT m)
userApi authres = mkCrud getUsers newUser getUser updateUser deleteUser
    where

        getUsers :: MonadIO m => ManagerT m [Entity User]
        getUsers = M.getUsers

        newUser :: MonadIO m => NewUser -> ManagerT m NoContent
        newUser nUser = do
          let nnUser = \t -> toUser nUser t
          _ <- M.newUser nnUser
          return NoContent
                        
        getUser :: MonadIO m => UserId ->  ManagerT m (Maybe (Entity User))
        getUser = M.getUser

        updateUser :: MonadIO m => UserId -> NewUser -> ManagerT m NoContent
        updateUser id updatedUser = do
          let updatedUserF = \t -> toUser updatedUser t
          _ <- M.updateUser id updatedUserF
          return NoContent

        deleteUser :: MonadIO m => UserId -> ManagerT m NoContent
        deleteUser id = fmap (\_ -> NoContent) $ M.deleteUser id
