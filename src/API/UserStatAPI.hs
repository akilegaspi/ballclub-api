{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}

module API.UserStatAPI where

import ClassyPrelude hiding (delete, id)
import Data.Aeson (FromJSON)
-- API Creation Modules
import Backbone (CrudAPI, mkCrud)

-- Database Modules
import Manager
import qualified Manager.UserStat as M
import Database.Persist.Sql
import Model

-- Server Type
import Servant.Auth.Server (AuthResult)
import Servant.Server (ServerT)
import Servant.API

data NewUserStat = NewUserStat deriving (Generic)
instance FromJSON NewUserStat

type UserStatAPI = UserStatCrudAPI
type UserStatCrudAPI = CrudAPI NewUserStat UserStat UserStatId

userStatApi :: MonadIO m => AuthResult User -> ServerT UserStatCrudAPI (ManagerT m)
userStatApi authres = mkCrud getUserStats newUserStat getUserStat updateUserStat deleteUserStat
    where
        getUserStats :: MonadIO m => ManagerT m [Entity UserStat]
        getUserStats = M.getUserstats

        newUserStat :: MonadIO m => NewUserStat -> ManagerT m NoContent
        newUserStat _ = pure NoContent
                        
        getUserStat :: MonadIO m => UserStatId ->  ManagerT m (Maybe (Entity UserStat))
        getUserStat = M.getUserstat

        updateUserStat :: MonadIO m => UserStatId -> NewUserStat -> ManagerT m NoContent
        updateUserStat id _ = pure NoContent

        deleteUserStat :: MonadIO m => UserStatId -> ManagerT m NoContent
        deleteUserStat id = pure NoContent

