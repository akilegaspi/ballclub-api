{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module API.GameAPI where

import           ClassyPrelude                  hiding (delete, id)
import           Data.Aeson (FromJSON)

-- API Creation Modules
import Backbone (CrudAPI, mkCrud)

import Database.Persist.Sql (Entity)
import Manager
import qualified Manager.Game as M
import Model

-- Server Type
import Servant.Auth.Server (AuthResult)
import Servant.Server (ServerT)
import Servant.API

data NewGame = NewGame { locationId :: LocationId
                       , schedule :: UTCTime
                       , team1 :: Maybe TeamId
                       , users :: [UserId] } deriving (Eq, Generic, Show)

instance FromJSON NewGame


toGame :: NewGame -> Game
toGame NewGame{..} = Game locationId schedule Nothing team1 Nothing

type GameAPI = GameCrudAPI
type GameCrudAPI = CrudAPI NewGame Game GameId

gameApi :: MonadIO m =>  AuthResult User -> ServerT GameCrudAPI (ManagerT m)
gameApi authRes = mkCrud getGames newGame getGame updateGame deleteGame
    where
        getGames :: MonadIO m => ManagerT m [Entity Game]
        getGames = M.getGames

        newGame :: MonadIO m => NewGame -> ManagerT m NoContent
        newGame game = do
          let nGame = toGame game
          _ <- M.newGame nGame
          return NoContent
                        
        getGame :: MonadIO m => GameId ->  ManagerT m (Maybe (Entity Game))
        getGame id = M.getGame id

        updateGame :: MonadIO m => GameId -> NewGame -> ManagerT m NoContent
        updateGame id updatedGame = do
          let uGame = toGame updatedGame
          _ <- M.updateGame id uGame
          return NoContent

        deleteGame :: MonadIO m => GameId -> ManagerT m NoContent
        deleteGame id = do
          _ <- M.deleteGame id
          return NoContent
