{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

module Backbone where

import ClassyPrelude hiding (Handler, delete, id)
import Control.Monad.Logger (NoLoggingT)
import Control.Monad.Trans.Reader (ReaderT)
import Control.Monad.Trans.Resource.Internal (ResourceT)

import Database.Persist (Entity)
import Database.Persist.Sql
import Servant

-- | Crud Generalization for API definition
type CrudAPI newEntity savedEntity entityId = (
  Get '[JSON] [Entity savedEntity]
  :<|> ReqBody '[JSON] newEntity :> PostNoContent '[JSON] NoContent
  :<|> Capture "id" entityId :> (
      Get '[JSON] (Maybe (Entity savedEntity))
      :<|> ReqBody '[JSON] newEntity :> PutNoContent '[JSON] NoContent
      :<|> DeleteNoContent '[JSON] NoContent
    ))

-- | CRUD API is generated for an entity by passing
-- | the functions for that entity
mkCrud :: Monad m => m [Entity aa]
       -> (a -> m NoContent)
       -> (id -> m (Maybe (Entity aa)))
       -> (id -> a -> m NoContent)
       -> (id -> m NoContent)
       -> ServerT (CrudAPI a aa id) m
mkCrud getAll newA getA updateA deleteA =
  getAll :<|> newA :<|> aOpts
    where
      aOpts idA = getA idA :<|> updateA idA :<|> deleteA idA


-- USAGE
-- userApi :: MonadIO m => Connection Pool -> ServerT (CrudAPI User UserId) m
-- userApi = mkCrudv2
{-
mkCrudv2 :: forall m a id. MonadIO m => ConnectionPool -> ServerT (CrudAPI a id) m
mkCrudv2 pool =
  getAll :<|> newA :<|> aOpts
      where
        aOpts idA = getA idA :<|> updateA idA :<|> deleteA idA

        runSql :: MonadIO m  => ReaderT SqlBackend (NoLoggingT (ResourceT IO)) a -> m a
        runSql query = liftSqlPersistMPool query pool

        getAll :: m [Entity a]
        getAll = runSql $
                 selectList [] []

        newA :: a -> m NoContent
        newA a = runSql $ do
            _ <- insert a
            return NoContent

        getA :: id -> m a
        getA idx = runSql $
                  selectFirst [id ==. idx] []

        updateA :: id -> a -> m NoContent
        updateA idx updatedA = runSql $ do
            replace idx updateA
            return NoContent

        deleteA :: id -> m NoContent
        deleteA idx = runSql $ do
          delete idx
          return NoContent

-}
